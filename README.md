# WP Typicode
WP Typicode is a wordpress plugin that can retrieve user data from https://jsonplaceholder.typicode.com. See who the user is and see the details.

## Installation
You can install this plugin by cloning this repo in the wp-content/plugins/ folder. Then activation via the admin dashboard.

When it is active, the WP Typicode menu will appear in the dashboard menu. Please click it.