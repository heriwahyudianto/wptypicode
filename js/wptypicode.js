function userList() {
	fetch('https://jsonplaceholder.typicode.com/users').then(function (response) {
		// The API call was successful!
		if (response.ok) {
			return response.json();
		} else {
			return Promise.reject(response);
		}
	}).then(function (data) {
		// This is the JSON from our response
		let users = ''
		data.forEach((item, index) => {
			users += '<tr><td class="tg-0lax"><a href="#" onClick="getDetail('+ item.id +');">'+
				item.id
			+ '</a></td><td class="tg-0lax"><a href="#" onClick="getDetail('+ item.id +');">' + 
				item.name
			+ '</a></td><td class="tg-0lax"><a href="#" onClick="getDetail('+ item.id +');">' + 
				item.username
			+ '</a></td></tr>'; 
		});
		document.getElementById('table-cnt').innerHTML = users;
	}).catch(function (err) {
		// There was an error
		console.warn('Something went wrong.', err);
	});
}
userList();

function getDetail(id) {
	showDetail();
	fetch('https://jsonplaceholder.typicode.com/users/' + id).then(function (response) {
		// The API call was successful!
		if (response.ok) {
			return response.json();
		} else {
			return Promise.reject(response);
		}
	}).then(function (data) {
		// This is the JSON from our response
		let user = ''
		user += '<tr><td class="tg-0lax">ID</td><td class="tg-0lax">' + data.id + '</td></tr>';
		user += '<tr><td class="tg-0lax">Name</td><td class="tg-0lax">' + data.name + '</td></tr>'; 
		user += '<tr><td class="tg-0lax">User Name</td><td class="tg-0lax">' + data.username + '</td></tr>'; 
		user += '<tr><td class="tg-0lax">Phone</td><td class="tg-0lax">' + data.phone + '</td></tr>';
		user += '<tr><td class="tg-0lax">Email</td><td class="tg-0lax">' + data.email + '</td></tr>'; 
		user += '<tr><td class="tg-0lax">Website</td><td class="tg-0lax">' + data.website + '</td></tr>';
		user += '<tr><td class="tg-0lax">Company</td><td class="tg-0lax">' + data.company.name + '<br>'+ 
			data.company.catchPhrase + '<br>' +
			data.company.bs +
			'</td></tr>';
		user += '<tr><td class="tg-0lax">Address</td><td class="tg-0lax">' + data.address.street + ', ' + data.address.suite + '<br>'+ 
			data.address.city + ', ' + data.address.zipcode + '<br>' +
			'<img src="'+ imagesUrl + 'images/place.png" class="text-img" /> ' + data.address.geo.lat + ', ' + data.address.geo.lng +
			'</td></tr>';
		document.getElementById('detail-cnt').innerHTML = user;
	}).catch(function (err) {
		// There was an error
		console.warn('Something went wrong.', err);
	});
}

function hideDetail() {
	document.getElementById('user-detail').style.display = "none";
	document.getElementById('user-list').style.display = "block";	
	document.getElementById('detail-cnt').innerHTML = '<tr><td colspan="2"><img src="' +
	imagesUrl + 'images/loading.gif" class="load-img" /></td></tr>';
}

function showDetail() {
	document.getElementById('user-detail').style.display = "block";
	document.getElementById('user-list').style.display = "none";
}