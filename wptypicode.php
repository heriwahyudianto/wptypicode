<?php
/**
 * Plugin Name:       Wp Typicode
 * Plugin URI:        http://github.com/heriwahyudianto/wptypicode
 * Description:       Get data from https://jsonplaceholder.typicode.com/users
 * Version:           1.0.0
 * Requires at least: 3.0
 * Requires PHP:      5.2
 * Author:            Heri Wahyudianto
 * Author URI:        http://heriwahyudianto.github.io/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * 
 * Wptypicode is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * any later version.
 *  
 * Wptypicode is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License
 * along with Wptypicode. If not, see https://www.gnu.org/licenses/gpl-2.0.html.
 */

add_action( 'admin_menu', 'wptypicode_page' );
function wptypicode_page() {
    add_menu_page(
        'wptypicode',
        'Wp Typicode',
        'manage_options',
        plugin_dir_path(__FILE__) . 'view.php',
        null,
        plugin_dir_url(__FILE__) . 'images/people.png',
        20
    );
}

function enqueue_scripts() {
    wp_enqueue_script( 'wptypicode', plugin_dir_url( __FILE__ ) . 'js/wptypicoe.js', array(), '1.0.0', true );
    wp_enqueue_script( 'table', plugin_dir_url( __FILE__ ) . 'js/table.js', array(), '1.0.0', true );
    wp_enqueue_style( 'wptypicode', plugin_dir_url( __FILE__ ) . 'css/wptypicode.css' );
}
add_action( 'wp_enqueue_scripts', 'enqueue_scripts');
 
function admin_enqueue_scripts() {
    wp_enqueue_script( 'wptypicode', plugin_dir_url( __FILE__ ) . 'js/wptypicode.js', array(), '1.0.0', true );
    wp_enqueue_script( 'table', plugin_dir_url( __FILE__ ) . 'js/table.js', array(), '1.0.0', true );
    wp_enqueue_style( 'wptypicode', plugin_dir_url( __FILE__ ) . 'css/wptypicode.css' );
}
add_action( 'admin_enqueue_scripts', 'admin_enqueue_scripts');

?>