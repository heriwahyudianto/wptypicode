<div id="user-list">
    <h1 class="text-center">User List</h1>
    <div class="tg-wrap">
        <table id="tg-mcvoN" class="tg">
            <thead>
                <tr>
                    <th class="tg-ul38">ID</th>
                    <th class="tg-ul38">Name</th>
                    <th class="tg-ul38">Username</th>
                </tr>
            </thead>
            <tbody id="table-cnt">
                <tr><td colspan="3"><img src="<?php echo plugin_dir_url( __FILE__ ); ?>images/loading.gif" class="load-img" />
                </td></tr>
            </tbody>
        </table>
    </div>
</div>
<div id="user-detail" class="display-none">
    <h1 class="text-center">User Detail</h1>
    <div class="tg-wrap">
        <table id="tg-mcvoN-detail" class="tg">
            <tbody id="detail-cnt">
                <tr><td colspan="2"><img src="<?php echo plugin_dir_url( __FILE__ ); ?>images/loading.gif" class="load-img" />
                </td></tr>
            </tbody>
        </table>
    </div>
    <button type="button" class="button text-center center-btn" onClick="hideDetail();">Back to list</button>
</div>
<script type="text/javascript">let imagesUrl = "<?php echo plugin_dir_url( __FILE__ ); ?>"</script>